#!/bin/bash

# Variables
export INSTALL_K3S_VERSION="v1.29.0+k3s1"

echo "k3s master node installation - version ${INSTALL_K3S_VERSION}"

# install master node

curl -sfL https://get.k3s.io | sh -

echo "checking service state"

sudo systemctl status k3s > k3s.service_check.txt
head -n 10 k3s.service_check.txt

echo "checking k3s nodes"

sudo kubectl get nodes -o wide

echo "master node installed successfully"
