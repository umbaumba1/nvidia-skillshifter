#!/bin/bash

# Variables
k3s_url="https://k3s-master:6443"
k3s_token="K1010ed23c825481574f1a1cf203ff30c4df6cf749e4882b4226aee643dc4db860a::server:96912fc75414514d2dd07b7490cb7272"
export INSTALL_K3S_VERSION="v1.29.0+k3s1"

echo "k3s worker node installation - version ${INSTALL_K3S_VERSION}"

# install worker1 node
curl -sfL https://get.k3s.io | K3S_URL=${k3s_url} K3S_TOKEN=${k3s_token} sh -

echo "checking service state"

sudo systemctl status k3s-agent > k3s-agent.service_check.txt
head -n 10 k3s-agent.service_check.txt

echo "k3s worker node installed successfully"
