# k3s deployment steps by umbaumba

Lightweight Kubernetes: k3s.io

Version: v1.29.0+k3s1

You need to deploy servers with at least 2 vCPUs and 2GB of RAM to be able to run this particular k3s environment.

## Alter hostnames 

Alter hostnames as follows

master node

**sudo hostnamectl set-hostname k3s-master**

worker1 node

**sudo hostnamectl set-hostname k3s-worker1**

worker2 node

**sudo hostnamectl set-hostname k3s-worker2**


## Update hosts file

Update hosts file with private IPv4 addresses for all 3 cluster nodes. 

Public address may get changed during server reboot.

**sudo vi /etc/hosts**

"your host private IPv4 address"    k3s-master

"your host private IPv4 address"    k3s-worker1

"your host private IPv4 address"    k3s-worker2


## Deploy master node

Deploy master node using **k3s_master_install.sh**.


## Deploy worker nodes
The K3S_TOKEN is created at **/var/lib/rancher/k3s/server/node-token** on master node.
To get the token use **sudo cat /var/lib/rancher/k3s/server/node-token**
Copy the token and update **k3s_worker_install.sh**.

Run **k3s_worker_install.sh** on dedicated 2 worker servers.

## Check your cluster
Once all nodes are deployed run **sudo kubectl get nodes** command and check your cluster availability.

## Uninstallation

### Master node uninstallation
Run **k3s-uninstall.sh** located in **/usr/local/bin/** directory.


### Worker node uninstallation
Run **k3s-agent-uninstall.sh** located in **/usr/local/bin/** directory.

